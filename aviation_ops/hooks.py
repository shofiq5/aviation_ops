# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from . import __version__ as app_version

app_name = "aviation_ops"
app_title = "Aviation Ops"
app_publisher = "Muhammad Shofiqul Islam"
app_description = "Aviation master, ops data"
app_icon = "green"
app_color = "green"
app_email = "shofiq5@gmail.com"
app_license = "MIT"

# Includes in <head>
# ------------------

# include js, css files in header of desk.html
# app_include_css = "/assets/aviation_ops/css/aviation_ops.css"
# app_include_js = "/assets/aviation_ops/js/aviation_ops.js"

# include js, css files in header of web template
# web_include_css = "/assets/aviation_ops/css/aviation_ops.css"
# web_include_js = "/assets/aviation_ops/js/aviation_ops.js"

# include js in page
# page_js = {"page" : "public/js/file.js"}

# include js in doctype views
# doctype_js = {"doctype" : "public/js/doctype.js"}
# doctype_list_js = {"doctype" : "public/js/doctype_list.js"}
# doctype_tree_js = {"doctype" : "public/js/doctype_tree.js"}
# doctype_calendar_js = {"doctype" : "public/js/doctype_calendar.js"}

# Home Pages
# ----------

# application home page (will override Website Settings)
# home_page = "login"

# website user home page (by Role)
# role_home_page = {
#	"Role": "home_page"
# }

# Website user home page (by function)
# get_website_user_home_page = "aviation_ops.utils.get_home_page"

# Generators
# ----------

# automatically create page for each record of this doctype
# website_generators = ["Web Page"]

# Installation
# ------------

# before_install = "aviation_ops.install.before_install"
# after_install = "aviation_ops.install.after_install"

# Desk Notifications
# ------------------
# See frappe.core.notifications.get_notification_config

# notification_config = "aviation_ops.notifications.get_notification_config"

# Permissions
# -----------
# Permissions evaluated in scripted ways

# permission_query_conditions = {
# 	"Event": "frappe.desk.doctype.event.event.get_permission_query_conditions",
# }
#
# has_permission = {
# 	"Event": "frappe.desk.doctype.event.event.has_permission",
# }

# Document Events
# ---------------
# Hook on document methods and events

# doc_events = {
# 	"*": {
# 		"on_update": "method",
# 		"on_cancel": "method",
# 		"on_trash": "method"
#	}
# }

# Scheduled Tasks
# ---------------

# scheduler_events = {
# 	"all": [
# 		"aviation_ops.tasks.all"
# 	],
# 	"daily": [
# 		"aviation_ops.tasks.daily"
# 	],
# 	"hourly": [
# 		"aviation_ops.tasks.hourly"
# 	],
# 	"weekly": [
# 		"aviation_ops.tasks.weekly"
# 	]
# 	"monthly": [
# 		"aviation_ops.tasks.monthly"
# 	]
# }

# Testing
# -------

# before_tests = "aviation_ops.install.before_tests"

# Overriding Whitelisted Methods
# ------------------------------
#
# override_whitelisted_methods = {
# 	"frappe.desk.doctype.event.event.get_events": "aviation_ops.event.get_events"
# }

