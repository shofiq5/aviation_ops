# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from frappe import _

def get_data():
	return [
		{
			"module_name": "Aviation Ops",
			"color": "green",
			"icon": "green",
			"type": "module",
			"label": _("Aviation Ops")
		}
	]
